﻿using System;

namespace Zahlen_Subtrahieren
{
    class Program
    {
        static void Main(string[] args)
        {
            int zahl1;
            int zahl2;

            eva();

            void eva()
            {
                eingabe();
                verarbeitung();
                ausgabe();
            }

            void eingabe()
            {
                //Eingabe Zahl 1
                Console.WriteLine("Bitte geben sie die erste Zahl ein:");
                zahl1 = int.Parse(Console.ReadLine());
                Console.WriteLine("Die erste Zahl lautet: " + zahl1);
                //Eingabe Zahl 2
                Console.WriteLine("Bitte geben sie die zweite Zahl ein: ");
                zahl2 = int.Parse(Console.ReadLine());
                Console.WriteLine("Die zweite Zahl lautet: " + zahl2);
            }

            void verarbeitung()
            {
                while (zahl1 != zahl2)
                {
                    if (zahl1 > zahl2)
                    {
                        Console.WriteLine("Rechnung: " + zahl1 + " - " + zahl2);
                        zahl1 = zahl1 - zahl2;
                        Console.WriteLine("Ergebnis: " + zahl1);
                    }
                    else
                    {
                        Console.WriteLine("Rechnung: " + zahl2 + " - " + zahl1);
                        zahl2 = zahl2 - zahl1;
                        Console.WriteLine("Ergebnis: " + zahl2);
                    }
                }
            }

            void ausgabe()
            {
                Console.WriteLine("Die Ergebnisse lauten: " + zahl1 + ", " + zahl2);
            }
        }
    }
}



